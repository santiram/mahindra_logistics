import cv2
import numpy as np


def verify_stamp(img):
    blue = np.uint8([[[255, 0, 0]]])
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    hsv_blue = cv2.cvtColor(blue, cv2.COLOR_BGR2HSV)[0][0][0]

    BLUE_MIN = np.uint8([hsv_blue-10, 40, 40])
    BLUE_MAX = np.uint8([hsv_blue+10, 255, 255])

    frame = cv2.inRange(hsv, BLUE_MIN, BLUE_MAX)

    contours, _ = cv2.findContours(
        frame, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

    if len(contours) > 25:
        return True
    else:
        return False


