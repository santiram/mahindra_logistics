# Detects text in a document stored in an S3 bucket. Display polygon box around text and angled text
import boto3
import io
import sys
from PIL import Image


def process_text_detection(image_path):

    client = boto3.client('textract')

    image = Image.fromarray(image_path)

    stream = io.BytesIO()
    image.save(stream, format="JPEG")
    image_binary = stream.getvalue()

    response = client.detect_document_text(Document={'Bytes': image_binary})
    response = client.analyze_document(
        Document={'Bytes': image_binary}, FeatureTypes=["TABLES", "FORMS"])

    # Get the text blocks
    return response['Blocks']


def get_status(blocks):
    status = {'Address': False,  'Consignor': False, 'Consignee': True}
    keywords = ['From', 'To', 'PlEASE', 'AT']
    for idx, block in enumerate(blocks):
        if block['BlockType'] == "WORD":
            if block['Text'] == 'Address' or block['Text'].startswith('Consignor') or block['Text'].startswith('Consignee'):
                if blocks[idx + 1]['Text'] not in keywords:
                    status[block['Text']] = True
    return status


def manual_intersection(image):
    blocks = process_text_detection(image)
    t = get_status(blocks)
    datas_list = []
    for key , value in t.items():
        # print(item)
        datas_list.append({"key": key ,"value":value})



    return datas_list
    # print("Blocks detected: " + str(block_count))


if __name__ == "__main__":
    main()
