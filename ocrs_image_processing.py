from utils import red_color_detection_check_for_manual , blur_text_check_ ,Download_image_from_blob
import easyocr
import cv2
# from args import image_path ,alpha , beta
from ApiModules import Azure_Calling_API , posting_Information
from details import details_dict
from stamp import verify_stamp
from manual import manual_intersection
import pandas as pd


#function for loading the images 

def load_image(image_path , use_image_processing = False): #later we change this code to the blob image download in python 
    img = cv2.imread(image_path)
    if use_image_processing:
        global alpha , beta
        img = increase_contrast(img,alpha ,beta)
        return img
    else:
        return img , url

def Process_image():
    #getting Image 
    
    excel_data_df = pd.read_excel('/home/sr/Downloads/Results.xlsx')
    ids = excel_data_df["Id"].tolist()
    fileurl = excel_data_df["FILEURL"].tolist()

    exceldf2 = pd.read_excel('/home/sr/Documents/manual2.xlsx')
    find_ids = exceldf2["Id"].tolist()

    for i in range(112 , 150):
        try:
            data = {"PODId": "414fb983-6db5-4513-a038-456e62acdc80",
                "SiteId": "MahindraSite",
                "FileUrl": "21344",
                "DocumentType": "SystemGenerated",
                "isBlur": False,
                "isStampPresent": False,
                "isSignaturePresent": False,
                "Status": "Completed", 
                "ScanDetails":[]
                }

            # podID , url = load_image(image_path)
            # podID = ids[i]
            # url = fileurl[i]
            podID = "f66131c6-a007-4746-92ca-c04fa426583f"
            url = "https://iwizardsiotcommon.blob.core.windows.net/development/1595087793521-25741-31759769.jpg"
            # not_present = {}

            if podID != None:
                data["PODId"] = podID
            if url != None:
                data["FileUrl"] = url
                print("Downloading from " , url)
                image = Download_image_from_blob(url)
                print("detecting the color")
                # if red_color_detection_check_for_manual(image):
                if podID in find_ids:
                    data["DocumentType"] = "Manual"
                    t = manual_intersection(image)
                    data["ScanDetails"] = t
                    data["Status"] = "Completed"
                    if verify_stamp(image):
                        data["isStampPresent"] = True

                else:
                    # print("Checking of for blur")

                    detail , data_list = details_dict(image)
                    print("Verifying the stamp")

                    if verify_stamp(image):
                        data["isStampPresent"] = True

                    if detail.get("MissingColumns") is not None:
                        data["Status"] = "NotReadable"
                    else:
                        data["Status"] = "Completed"

                    
                        #then stump check 
                    data["ScanDetails"] = data_list
                    
            print("This is the details ")
            # print(data)
            # posting_Information([data])
        except Exception as exc:
            print("This url is not downloadavble ",exc.args)

if __name__ == "__main__":
    Process_image()
