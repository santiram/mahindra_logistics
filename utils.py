import cv2
import numpy as np 
import pytesseract
import easyocr
import urllib
import numpy as np
from io import StringIO


#function for detecting whether the red collor is present or not 

def red_color_detection_check_for_manual(image):
    result = image.copy()
    result = image[40:140 , 0:]
    image = image[40:140 , 0:]
    image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    lower = np.array([155,25,0])
    upper = np.array([179,255,255])
    mask = cv2.inRange(image, lower, upper)
    result = cv2.bitwise_and(result, result, mask=mask)
    result = np.asarray(result)
    # count = np.count_nonzero(result)
    # print(count)
    # cv2.imshow("Image" , result)
    # if cv2.waitKey(0) & 0xFF == ord('q'):
    #     cv2.destroyAllWindows()
    count = 0
    for data in result:
        for datas in data:
            for d in datas:
                if d > 50 and d<90:
                    count+=1
    
    print(count)
    if count > 50:
        return True 
    else:
        return False



#function to check whether the image is blur or not 
#returns true if the image is blur else return false 

def blur_text_check_(img):
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    try :
        labels =pytesseract.image_to_osd(img)
        labels = labels.split('\n')
        confidence = float(labels[5].split(":")[1].strip())
        print("Confidence is thee " , confidence)
        return False
    except:
        return True



# fucntion for processing the images 

def increase_contrast(image ,alpha ,beta):

    new_image = np.zeros(image.shape, image.dtype)
    for y in range(image.shape[0]):
        for x in range(image.shape[1]):
            for c in range(image.shape[2]):
                new_image[y,x,c] = np.clip(alpha*image[y,x,c] + beta, 0, 255)


    return new_image


def Loading_ocrs_model():
    reader = easyocr.Reader(['en'])
    global image_path 
    img  =  load_image(image_path )
    bounds  = reader.readtext(img)
    print_bounds(bounds)


#for finding the laplacian 

def variance_of_laplacian(image):
	# compute the Laplacian of the image and then return the focus
	# measure, which is simply the variance of the Laplacian
	return cv2.Laplacian(image, cv2.CV_64F).var()


# function for downloading the images using the blob url  las 

def Download_image_from_blob(url):
    img = None
    with urllib.request.urlopen(url) as resp:
        img = np.fromstring(resp.read(), dtype="uint8")
    img_np = cv2.imdecode(img, cv2.IMREAD_COLOR)
    return img_np