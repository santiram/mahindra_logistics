# Detects text in a document stored in an S3 bucket. Display polygon box around text and angled text
import boto3
import io
import sys
import re
from PIL import Image




def process_text_detection(image):

    client = boto3.client('textract')

    # image_path = 'System Generated POD/1488.jpg'
    image = Image.fromarray(image)

    stream = io.BytesIO()
    image.save(stream, format="JPEG")
    image_binary = stream.getvalue()

    response = client.detect_document_text(Document={'Bytes': image_binary})

    # Get the text blocks
    blocks = response['Blocks']
    return blocks


def get_address(blocks,details):
    start = False
    end = False
    field = True
    address = ['', '']
    for block in blocks:
        if block['BlockType'] == 'LINE':
            if block['Text'].startswith('Address'):
                start = True
            if block['Text'] == 'To':
                end = True
            if start and len(block['Relationships'][0]['Ids']) > 1:
                if field:
                    address[0] += block['Text'] + ' '
                    field = False
                else:
                    address[1] += block['Text'] + ' '
                    field = True
            elif end and len(block['Relationships'][0]['Ids']) == 1:
                if block['Text'] == 'Consignor':
                    return {'Consignor address': address[0][8:], 'Consignee address': address[1][8:]}


def get_names(blocks,details):
    for block in blocks:
        if block['BlockType'] == 'LINE':
            for k in details.keys():
                if block['Text'].startswith(k) and len(block['Relationships'][0]['Ids']) > 1:
                    # print(block['Text'])
                    details[k] = block['Text'][len(k) + 1:]


def get_date_and_gcn(blocks,details):
    values = {}
    regex_Date = "[1-3][0-9]-[a-z|A-Z]{3}-[1-2][0-9][0-9][0-9]"
    regex_Declared_VAlues = "[0-9]+[.][0-9]{2}"
    for ind, block in enumerate(blocks):
        if block['BlockType'] == 'LINE':
            if block['Text'].startswith('G.C.N') and len(block['Relationships'][0]['Ids']) == 1:
                values['GCN No.'] = blocks[ind + 1]['Text']
            if re.match(regex_Date, block['Text']):
                values['Date'] = blocks[ind]['Text']
            if re.match(regex_Declared_VAlues ,block["Text"]):
                values["Declared_Values"] = block[ind]['Text']
            # if len(values) == 3:
    return values


def get_table(blocks,details):
    for ind, block in enumerate(blocks):
        if block['BlockType'] == 'LINE':
            if block['Text'].startswith('Invo') and len(block['Relationships'][0]['Ids']) == 2:
                data = [x['Text'] for x in blocks[ind: ind+8]]
                if not data[3].startswith('Remarks'):
                    return {data[0]: data[3], data[1]: data[4],
                            data[2][:-7]: data[5], 'Remarks': data[6]}
                else:
                    return {data[0]: data[4], data[1]: data[5],
                            data[2]: data[6], data[3]: data[7]}


def details_dict(image):
    details = {'Service Mode': '', 'Gross Wt': '',
           'Freight': '', 'Chargeable Wt': '', 'Consignor': '', 'Consignee': ''}
    # if len(sys.argv) != 2:
    #     print('Pass only one image path')
    #     exit(1)
    blocks = process_text_detection(image )
    print(blocks)
    try:
        get_names(blocks , details)
    except:
        pass
    try:
        details.update(get_address(blocks,details))
    except:
        pass
    try:
        details.update(get_table(blocks,details))
    except:
        pass
    try:
        details.update(get_table(blocks,details))
    except:
        pass

    try :
        details.update(get_date_and_gcn(blocks,details))
    except:
        pass

    # not_present = {}
    if details.get("Consignor") == "":
        if details.get("MissingColumns") is None:
            details["MissingColumns"] = "Consignor"
        else:
            details["MissingColumns"] = str(details["MissingColumns"])+","+"Consignor"
            
    if details.get("Consignee") == "" :
        details["Consignee"]= "Consignee"
        if details.get("MissingColumns") is None:
            details["MissingColumns"] = "Consignee"
        else:
            details["MissingColumns"] = str(details["MissingColumns"])+","+"Consignee"

    if details.get("Consignor address") == "" or None:
        if details.get("MissingColumns") is None:
            details["MissingColumns"] = "Consignor address"
        else:
            details["MissingColumns"] = str(details["MissingColumns"])+","+"Consignor address"


    if details.get("Consignee address") == "" or None:
        if details.get("MissingColumns") is None:
            details["MissingColumns"] = "Consignee address"
        else:
            details["MissingColumns"] = str(details["MissingColumns"])+","+"Consignee address"
    
    if  details.get("Gross Wt") =="":
        if details.get("MissingColumns") is None:
            details["MissingColumns"] = "Gross Wt"
        else:
            details["MissingColumns"] = str(details["MissingColumns"])+","+"Gross Wt"

    if details.get("Chargeable Wt") == "":
        if details.get("MissingColumns") is None:
            details["MissingColumns"] = "Chargeable Wt"
        else:
            details["MissingColumns"] = str(details["MissingColumns"])+","+"Chargeable Wt"

    if details.get("Invoice No") == "" or None:
        if details.get("MissingColumns") is None:
            details["MissingColumns"] = "Invoice No"
        else:
            details["MissingColumns"] = str(details["MissingColumns"])+","+"Invoice No"

    if details.get("No of Pkg") == "" or None:
        if details.get("MissingColumns") is None:
            details["MissingColumns"] = "No of Pkg"
        else:
            details["MissingColumns"] = str(details["MissingColumns"])+","+"No of Pkg"

    if details.get("Declared_Values") == "" or None:
        if details.get("MissingColumns") is None:
            details["MissingColumns"] = "Declared_Values"
        else:
            details["MissingColumns"] = str(details["MissingColumns"])+","+"Declared_Values"


    datas_list = []

    for key , value in details.items():
        # if value == "not_readable":
        #     datas_list.append({"key":"not_readable" , "value":})
        # # print(item)

        datas_list.append({"key": key ,"value":value})

    return details , datas_list


if __name__ == "__main__":
    main()
